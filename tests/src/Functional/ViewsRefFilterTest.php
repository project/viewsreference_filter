<?php

namespace Drupal\Tests\viewsreference_filter\Functional;

use Drupal\KernelTests\AssertContentTrait;
use Drupal\Tests\BrowserTestBase;

/**
 * Test the Views Reference Field Filter.
 *
 * @group viewsreference_field
 */
class ViewsRefFilterTest extends BrowserTestBase {

  use AssertContentTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * A user with permission to bypass access content.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'viewsreference',
    'viewsreference_filter',
    'viewsreference_filter_test',
    'node',
    'user',
    'block',
    'views_ui',
    'field_ui',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->adminUser = $this->drupalCreateUser([
      'access content',
      'bypass node access',
      'administer nodes',
      'administer content types',
      'administer node fields',
      'administer node display',
      'administer node form display',
    ]);
    $this->drupalLogin($this->adminUser);

    $this->drupalGet('node/add/test_article');
    $this->submitForm([
      'title[0][value]' => 'Node #1 (Published)',
      'field_term_reference[0][target_id]' => 'Foo',
      'status[value]' => '1',
    ], 'Save');

    $this->drupalGet('node/add/test_article');
    $this->submitForm([
      'title[0][value]' => 'Node #2 (Unpublished)',
      'field_term_reference[0][target_id]' => 'Bar',
      'status[value]' => '0',
    ], 'Save');

  }

  /**
   * Test that the filters values selected are applied to the view.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   */
  public function testExposedFiltersApplication(): void {
    // Create a content that refers the Test view.
    $this->drupalGet('node/add/test_content_type');
    $this->submitForm([
      'title[0][value]' => 'Test',
      'field_views_reference_field[0][target_id]' => 'test_view',
    ], 'Save');
    $this->submitForm([
      'title[0][value]' => 'Test',
      'field_views_reference_field[0][target_id]' => 'test_view',
      'field_views_reference_field[0][display_id]' => 'block_1',
    ], 'Save');
    $this->drupalGet('node/3');

    // Test the results without setting a value.
    $this->assertSession()->pageTextContains('Node #1 (Published)');
    $this->assertSession()->pageTextContains('Node #2 (Unpublished)');

    // Test the results for published content.
    $this->drupalGet('node/3/edit');
    $this->submitForm([
      'title[0][value]' => 'Test',
      'field_views_reference_field[0][target_id]' => 'test_view',
      'field_views_reference_field[0][options][exposed_filters][status]' => '0',
    ], 'Save');

    $this->assertSession()->pageTextNotContains('Node #1 (Published)');
    $this->assertSession()->pageTextContains('Node #2 (Unpublished)');

    // Test the results for unpublished content.
    $this->drupalGet('node/3/edit');
    $this->submitForm([
      'title[0][value]' => 'Test',
      'field_views_reference_field[0][target_id]' => 'test_view',
      'field_views_reference_field[0][options][exposed_filters][status]' => '1',
    ], 'Save');

    $this->assertSession()->pageTextContains('Node #1 (Published)');
    $this->assertSession()->pageTextNotContains('Node #2 (Unpublished)');
  }

  /**
   * Test the visibility of the exposed filters and the user submitted input.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   */
  public function testExposedFiltersVisibility():void {
    // Create a content that refers the Test view.
    $this->drupalGet('node/add/test_content_type');
    $this->submitForm([
      'title[0][value]' => 'Test',
      'field_views_reference_field[0][target_id]' => 'test_view',
    ], 'Save');
    $this->submitForm([
      'title[0][value]' => 'Test',
      'field_views_reference_field[0][target_id]' => 'test_view',
      'field_views_reference_field[0][display_id]' => 'block_1',
    ], 'Save');
    $this->drupalGet('node/3');

    // Enable the "Show Filters on Page" option and set the Unpublished Status.
    $this->drupalGet('node/3/edit');
    $this->submitForm([
      'title[0][value]' => 'Test',
      'field_views_reference_field[0][target_id]' => 'test_view',
      'field_views_reference_field[0][options][exposed_filters][vr_exposed_filters_visible]' => '1',
      'field_views_reference_field[0][options][exposed_filters][status]' => '0',
    ], 'Save');

    // Check the visibility of the filters.
    $this->assertSession()->pageTextContains('Published status');
    $this->assertSession()->pageTextContains('Authored by');
    $this->assertSession()->pageTextContains('Term reference');

    // Check that the filters are working.
    $this->assertSession()->pageTextNotContains('Node #1 (Published)');
    $this->assertSession()->pageTextContains('Node #2 (Unpublished)');

    // Check the user input for the exposed visible filters.
    $this->submitForm([
      'status' => '1',
    ], 'Apply');
    $this->assertSession()->pageTextContains('Node #1 (Published)');
    $this->assertSession()->pageTextNotContains('Node #2 (Unpublished)');

    // Check an autocomplete filter.
    $this->submitForm([
      'uid' => 'Anonymous',
    ], 'Apply');
    $this->assertSession()->pageTextContains('No results');

    // Check the default value for an entity reference filter.
    $this->drupalGet('node/3/edit');
    $this->submitForm([
      'title[0][value]' => 'Test',
      'field_views_reference_field[0][target_id]' => 'test_view',
      'field_views_reference_field[0][options][exposed_filters][vr_exposed_filters_visible]' => '1',
      'field_views_reference_field[0][options][exposed_filters][status]' => 'All',
      'field_views_reference_field[0][options][exposed_filters][field_term_reference_target_id]' => 'Foo (1)',
    ], 'Save');
    $this->assertSession()->pageTextContains('Node #1 (Published)');
    $this->assertSession()->pageTextNotContains('Node #2 (Unpublished)');

    // Check the exposed filter.
    $this->submitForm([
      'field_term_reference_target_id' => 'Bar',
    ], 'Apply');
    $this->assertSession()->pageTextNotContains('Node #1 (Published)');
    $this->assertSession()->pageTextContains('Node #2 (Unpublished)');

    // Check the entity is still selected in the form edit.
    $this->drupalGet('node/3/edit');
    $this->assertSession()->fieldValueEquals('field_views_reference_field[0][options][exposed_filters][field_term_reference_target_id]', 'Foo (1)');

  }

}
