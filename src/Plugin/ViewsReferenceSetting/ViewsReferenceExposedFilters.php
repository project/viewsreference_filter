<?php

namespace Drupal\viewsreference_filter\Plugin\ViewsReferenceSetting;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\views\ViewExecutable;
use Drupal\viewsreference\Plugin\ViewsReferenceSettingInterface;
use Drupal\viewsreference_filter\ViewsRefFilterUtilityInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The views reference setting plugin for exposed filters, for editors.
 *
 * @ViewsReferenceSetting(
 *   id = "exposed_filters",
 *   label = @Translation("Exposed Filters - editor view"),
 *   default_value = "",
 * )
 */
class ViewsReferenceExposedFilters extends PluginBase implements ViewsReferenceSettingInterface, ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * The factory to load a view executable with.
   *
   * @var \Drupal\viewsreference_filter\ViewsRefFilterUtilityInterface
   */
  protected $viewsUtility;

  /**
   * Constructs a new ViewsReferenceExposedFilters object.
   *
   * @param array $configuration
   *   The configuration.
   * @param string $pluginId
   *   The plugin_id for the plugin instance.
   * @param mixed $pluginDefinition
   *   The plugin implementation definition.
   * @param \Drupal\viewsreference_filter\ViewsRefFilterUtilityInterface $viewsUtility
   *   The views reference filter utility.
   */
  public function __construct(array $configuration, $pluginId, $pluginDefinition, ViewsRefFilterUtilityInterface $viewsUtility) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->viewsUtility = $viewsUtility;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('viewsreference_filter.views_utility')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function alterFormField(&$form_field) {
    $view = $this->viewsUtility->loadView($this->configuration['view_name'], $this->configuration['display_id']);

    if (!$view) {
      $form_field = [];
      return;
    }

    $current_values = $form_field['#default_value'];
    unset($form_field['#default_value']);
    $form_field['#type'] = 'container';
    $form_field['#tree'] = TRUE;
    $form_field['vr_exposed_filters_visible'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show Filters on Page'),
      '#default_value' => (isset($current_values['vr_exposed_filters_visible']) && $current_values['vr_exposed_filters_visible']),
    ];

    // Some plugin may look into current exposed input to change some behavior,
    // i.e. setting a default value (see SHS for an example). So set current
    // values as exposed input.
    $view->setExposedInput($current_values);

    // If we are using the exposed form, render it.
    // So we can get the form elements to display.
    if ($view->display_handler->usesExposed()) {
      /** @var \Drupal\views\Plugin\views\exposed_form\ExposedFormPluginInterface $exposed_form */
      $exposed_form = $view->display_handler->getPlugin('exposed_form');
      $rendered_form = $exposed_form->renderExposedForm();
      foreach (['#info', '#theme'] as $el) {
        $form_field[$el] = $rendered_form[$el];
      }

      // Go through each handler and let it generate its exposed widget.
      // @see ViewExposedForm::buildForm()
      foreach ($view->display_handler->handlers as $type => $value) {
        /** @var \Drupal\views\Plugin\views\HandlerBase $handler */
        foreach ($view->$type as $handler) {
          if ($handler->canExpose() && $handler->isExposed()) {
            if ($info = $handler->exposedInfo()) {
              if (isset($rendered_form[$info['value']])) {
                $form_field[$info['value']] = $rendered_form[$info['value']];

                // Unset attributes that are not relevant for this context and
                // to ensure that submitted values are appropriately mapped
                // deep into the form_state tree structure.
                foreach ([
                  '#tree',
                  '#parents',
                  '#array_parents',
                  '#name',
                  '#processed',
                ] as $el) {
                  unset($form_field[$info['value']][$el]);
                }

                // Force the processing of the default value.
                $form_field[$info['value']]['#process_default_value'] = FALSE;

                // The handling of default values for exposed filters seems to
                // be really odd. Default values are mapped directly into #value
                // by the views handlers. This prevents us from properly
                // handling new user-submitted values.
                $form_field[$info['value']]['#default_value'] = $form_field[$info['value']]['#value'];
                unset($form_field[$info['value']]['#value']);
              }
            }
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function alterView(ViewExecutable $view, $values) {
    // Get exposed filter visibility, and remove configuration.
    $vrExposedFiltersVisible = FALSE;
    if (isset($values['vr_exposed_filters_visible'])) {
      $vrExposedFiltersVisible = $values['vr_exposed_filters_visible'];
      unset($values['vr_exposed_filters_visible']);
    }

    if (!empty($values) && is_array($values)) {
      $view_filters = $view->display_handler->getOption('filters');
      $filters = [];
      $filters_by_id = [];
      foreach ($view_filters as $key => $filter) {
        if (isset($filter['expose']['identifier'])) {
          $filters_by_id[$filter['expose']['identifier']] = $key;
        }
      }
      foreach ($values as $index => $value) {
        if ($value !== '' && isset($filters_by_id[$index])) {
          $filters[$index] = $value;
        }
      }
      if ($filters) {
        $filters_input = $view->getExposedInput();
        // Don't force the filters if the exposed filters are visible and there
        // is a user input.
        if (!$vrExposedFiltersVisible || empty($filters_input)) {
          if (!isset($filters_input['viewsreference'])) {
            $view->setExposedInput($filters);
          }
        }
      }
    }

    if (!$vrExposedFiltersVisible) {
      // Force exposed filters form to not display when rendering the view.
      $view->display_handler->setOption('exposed_block', TRUE);
    }
    else {
      $view->display_handler->setOption('exposed_block', FALSE);
    }
  }

}
