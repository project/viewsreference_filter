<?php

namespace Drupal\viewsreference_filter;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\views\ViewExecutableFactory;
use Psr\Log\LoggerInterface;

/**
 * The Views Reference Filter Utility Class.
 */
class ViewsRefFilterUtility implements ViewsRefFilterUtilityInterface {

  /**
   * A LoggerChannelInterface viewsreference_filter.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The views executable factory.
   *
   * @var \Drupal\views\ViewExecutableFactory
   */
  protected $viewsExecutableFactory;

  /**
   * Constructs a new ViewsRefFilterUtility object.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   A LoggerChannelInterface viewsreference_filter.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\views\ViewExecutableFactory $viewsExecutableFactory
   *   The views executable factory.
   */
  public function __construct(LoggerInterface $logger, EntityTypeManagerInterface $entity_type_manager, ViewExecutableFactory $viewsExecutableFactory) {
    $this->logger = $logger;
    $this->entityTypeManager = $entity_type_manager;
    $this->viewsExecutableFactory = $viewsExecutableFactory;
  }

  /**
   * {@inheritdoc}
   */
  public function loadView($view_name, $display_id) {
    static $view;

    if (!isset($view)) {
      if (!empty($view_name) && !empty($display_id)) {
        try {
          $view = $this->entityTypeManager
            ->getStorage('view')
            ->load($view_name);
          $view = $this->viewsExecutableFactory->get($view);
          $view->setDisplay($display_id);
          $view->initHandlers();
        }
        catch (\Exception $e) {
          $this->logger->error('Exception: @error', ['@error' => $e->getMessage()]);
        }
      }
      else {
        $this->logger->notice('Either the Views Name: %view_name or Display Id: %display_id were not set.', [
          '%view_name' => $view_name,
          '%display_id' => $display_id,
        ]);
      }
    }

    return $view;
  }

}
