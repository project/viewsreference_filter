# Views Reference Field Filter

## Introduction

This module has been created to give the content editor more flexibility to
control the output of the displayed view using the *Views Reference Field*
module.

It allows you to expose the view's exposed filters to the user by adding a
filter plugin.

This module born in response to:
[#3004636: Add View filter plugin to improve experience for content editor](https://www.drupal.org/project/viewsreference/issues/3004636)

## Requirements

This module requires the following modules:

- [Views Reference Field](https://www.drupal.org/project/viewsreference)

## Installation

* Install as you would normally install a contributed Drupal module.
  Visit [Installing modules](https://www.drupal.org/docs/extending-drupal/installing-modules)
  for further information.

## Configuration

To enable the functionality of this module:
1) Create a view with exposed filters.
2) Add a 'Views reference' field to an entity using the manage fields tab
3) Select the additional settings offered by the Views reference module.
4) Select the new **Exposed Filters - editor view** option,
   available in the "Enable extra settings".

## Maintainers and credits

* Robin Andrew - https://www.drupal.org/u/roborew
* Rodolfo Candido - https://www.drupal.org/u/pheudo

Most of the credit goes to Gabriele https://www.drupal.org/u/gambry
and all the others who did the initial work on this plugin.
